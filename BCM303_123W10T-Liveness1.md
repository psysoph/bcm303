**BCM 303-123 W10 tutorial**

**Dear Team,**

Let's start by writing a letter to any potential group that might take your project idea and develop it. Think of it as an invitation and a launchpad for them to begin developing that project - but also think of it as a chance to reflect as a group on the work you've done and how we might address the task moving forward.

What was behind your selection of the topic/subject and the content of your digital experience. What did you love about the project? What are the project's strengths? What do wish you'd done differently?

What resources can you offer an where might the group find them?

What opportunities can you imagine being pursued in the project's live iteration? How would you like to see the project developed?

**Framing Liveness**

John Cages 4'33'' demonstrates a number of different things about liveness and the structure of an event. That exercise is useful in highlighting the way a live experience and eventfulness are made from different fabrics (or at least a different balance of fabrics) to that of a purely mediated or representational experience. It is important to note however that this performance included performative and representational elements that set up and framed a very immediate experience.

Today we want to construct a 'shopping list' of those qualities of liveness or eventfulness and how they are structured or composed. What elements constitute or compose your favourite live experiences? What are all the examples of live experience you can think of and how are they constituted or composed?  Which of these elements would you like to aim for or borrow in the constitution of your novel experiment if liveness and event?

**You might think about:**

\- Media as 'mediated' experience -What is the opposite of **Mediation**...perhaps **Immediation**?....

Is it useful to think through both digital and live experiences as a balance of **mediation** and **immediation** ...or having ***mediate*** and ***immediate*** components that frame and contextualise and amplify each other..

You might also think through the balance and distinction between a **representational** and **experiential** mode of communication.

Blast Theory (https://www.blasttheory.co.uk/fringetix/rider-spoke-af2022) - The people that made my most affecting  live experience and its fine crafting of mediation/immediation, representational and experiential. 

\- Specifically, what are all the experiences we label 'live'? Such as music, stage performance, theatre, sport, radio, television (game show, variety, comedy), video streams, networked gaming, and social media... how do these mediums capitalise/exploit liveness (and do so differently)? What are they composed and constituted of.... what do we like about them? Does it have anything to do with us as participants sharing in the experience? That the experience is ours and not a re-presentation? That the experience is new and innovative? 

\- What about other kinds of mediated experiences that are 'live' by way of attending an event such as an art exhibition or even going to the cinema instead of Netflix at home - what makes those forums an event? - What role do event conventions play in the meaning of curating an experience? For example, the ritual of going to a rock concert or the frame of mind you bring to a museum or even the expectations of a university tutorial? 

Takes note and make lists...work ***live*** as much as possible..

**Collaboration and Event**

How can we extend the notion of *collaboration* to explore liveness and event. Have you experienced an art work in which the user's input was incorporated. Performance, Relational and socially engaged art  are all provide really interesting examples of using event, liveness, and collaboration as mediums of and art practice. How do these examples use collaboration, eventfulness and liveness to create complex and meaningful experiences?

Suzanne Lacy:

https://www.youtube.com/watch?v=ieBUiGF7684

https://www.youtube.com/watch?v=ie7A8F0D-k4. (This work addresses issues of sexual violence) 

https://www.youtube.com/watch?v=YcmcEZxdlv4 (The circle and the square)

Ambromovic:

https://www.youtube.com/watch?v=YcmcEZxdlv4

**More Explanation:**

You might also think through these examples/diagrams of mediate and immediate experience. You might ask what is (and how is it)  a meaningful experience? Compare:

'I read about a car accident in the newspaper'   vs 
'My friend was in a car accident' vs
 'I saw a car accident on the way here' vs 
'I was in a car accident' 

then consider the more nuanced mediated narratives

'I played the new Gran Turismo game but crashed the car and lost' vs 
'The movie Crash by David Cronenberg about a group of symphorophiliacs - people aroused by car accidents?' vs 
'I did this amazing VR crash simulator it was terrifying'

https://vimeo.com/503553421/7cafe9659e?embedded=false&source=video_title&owner=1950185 

\- What role is the media-ted space playing in our experience? Entertainment, enlightenment?, education? How might live sport be understood as a mediated experience? How does the mediate and immediate interact? Is sport representational? How? Is it experiential... how? In what ways?

\- What role does 'story' play in our experience? 

\- What provides meaning to our experience? Emotion? Relevance to personal lives/choices/ambitions/values? 

**Locking in and Development.**

Invent a game or activity of collaborative exploration based on the project you've chosen. It could be the invention of a song to be sung in the round, a parlour larp, a puzzle, a creative activity, an invitation to speculate, a framework for creative speculation, a listening or gathering activity, a guided walk, an interpretive/improvised dance.....

***The one proviso is that it must be meaningfully generative of content/collaboration in relation to your theme.***

If you 'take the piss' - the exercise will probably be useless...Write clear instructions and test your idea so we can execute these experiments next week.

